## Zero - the ultimate management of human resources (_HR_) ##

This is a heavily modified version of the original _Zero_ application
that entirely focuses on the management of human resources (_HR_).

Team members: Aivaras Džiaugys, Saulius Stankevičius, Šarūnas Šarakojis.
