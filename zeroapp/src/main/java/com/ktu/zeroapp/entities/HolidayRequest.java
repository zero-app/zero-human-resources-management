package com.ktu.zeroapp.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class HolidayRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private LocalDate start;
    @Column
    private LocalDate end;
    @Column
    private String comment;
    @Column
    private String condition;
    @Column
    private Long employeeId;

    public HolidayRequest() {

    }

    public HolidayRequest(LocalDate start, LocalDate end, String comment, String condition, Long employeeId) {
        this.start = start;
        this.end = end;
        this.comment = comment;
        this.condition = condition;
        this.employeeId = employeeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }
}
