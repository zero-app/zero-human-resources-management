package com.ktu.zeroapp.entities;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class Review {
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private LocalDate begining;
	@Column
	private LocalDate end;
	@Column
	private Integer improvement;
	@Column
	private Integer quality;
	@Column
	private Integer grade;
	@Column
	private String comment;
	@Column
	private LocalDate dateOfReview;
	@Column
	private Long employeeId;
	@Column
	private Long managerId;

	public Review() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBegining() {
		return begining;
	}

	public void setBegining(LocalDate begining) {
		this.begining = begining;
	}

	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}

	public Integer getImprovement() {
		return improvement;
	}

	public void setImprovement(Integer improvement) {
		this.improvement = improvement;
	}

	public Integer getQuality() {
		return quality;
	}

	public void setQuality(Integer quality) {
		this.quality = quality;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public LocalDate getDateOfReview() {
		return dateOfReview;
	}

	public void setDateOfReview(LocalDate dateOfReview) {
		this.dateOfReview = dateOfReview;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}
}
