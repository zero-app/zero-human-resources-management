package com.ktu.zeroapp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class RegistrationCode
{
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String code;
    @Column
    private Long employeeId;

    public RegistrationCode()
    {

    }

    public  RegistrationCode(String code, Long employeeId)
    {
        this.code = code;
        this.employeeId = employeeId;
    }

    public Long getId()
    {
        return id;
    }

    public Long getEmployeeId()
    {
        return employeeId;
    }

    public String getCode()
    {
        return code;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public void setEmployeeId(Long employeeId)
    {
        this.employeeId = employeeId;
    }
}
