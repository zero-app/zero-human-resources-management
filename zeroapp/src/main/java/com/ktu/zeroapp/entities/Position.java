package com.ktu.zeroapp.entities;

/**
 * Created by Aivaras on 23/05/2017.
 */
public enum Position {
    SANDELININKAS("Sandelininkas", 1),
    VADYBININKAS("Vadybininkas",2),
    SKYRIAUS_VADOVAS("Skyriaus vadovas", 3),
    KOMANDOS_VADAS("Komandos vadas",4),
    PAMAINOS_VADAS("Pamainos vadas",5),
    IMONES_VADOVAS("Imones vadovas",6),
    DIREKTORIUS("Direktorius",7);

    private String name;
    private Integer value;

    Position(String name, Integer value){
        this.name = name;
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
