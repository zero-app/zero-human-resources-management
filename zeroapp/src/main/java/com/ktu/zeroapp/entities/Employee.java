package com.ktu.zeroapp.entities;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
public class Employee {
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private Long personalIdentificationNumber;
	@Column
	private String bankAccount;
	@Column
	private String address;
	@Column
	private String phoneNumber;
	@Column
	private String condition;
	@Column
	private Double salary;
	@Column
	private Double holiday;
	@Column
	private LocalDate startedWork;
	@Column
	private Long blacklistedEmployeeId;
	@Column
	private String email;
	@Column
	private String position;

	public Employee() {
	}

	public Employee(String firstName, String lastName, Long personalIdentificationNumber, String bankAccount, String address, String phoneNumber, String condition, Double salary, Double holiday, LocalDate startedWork, String email, String position) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.personalIdentificationNumber = personalIdentificationNumber;
		this.bankAccount = bankAccount;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.condition = condition;
		this.salary = salary;
		this.holiday = holiday;
		this.startedWork = startedWork;
		this.email = email;
		this.position = position;
	}

	public Long getBlacklistedEmployeeId() {
		return blacklistedEmployeeId;
	}

	public void setBlacklistedEmployeeId(Long blacklistedEmployeeId) {
		this.blacklistedEmployeeId = blacklistedEmployeeId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getPersonalIdentificationNumber() {
		return personalIdentificationNumber;
	}

	public void setPersonalIdentificationNumber(Long personalIdentificationNumber) {
		this.personalIdentificationNumber = personalIdentificationNumber;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Double getHoliday() {
		return holiday;
	}

	public void setHoliday(Double holiday) {
		this.holiday = holiday;
	}

	public LocalDate getStartedWork() {
		return startedWork;
	}

	public void setStartedWork(LocalDate startedWork) {
		this.startedWork = startedWork;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getEmail()
	{
		return email;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
}
