package com.ktu.zeroapp.entities;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.*;

@Entity
public class BlacklistedEmployee {
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private LocalDate blacklistedOn;
	@Column
	private String comment;
	@Column
	private Long employeeId;

	public BlacklistedEmployee() {
	}

	public BlacklistedEmployee(LocalDate blacklistedOn, String comment, Long employeeId) {
		this.blacklistedOn = blacklistedOn==null? LocalDate.now() : blacklistedOn;
		this.comment = comment;
		this.employeeId = employeeId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public LocalDate getBlacklistedOn() {
		return blacklistedOn;
	}

	public void setBlacklistedOn(LocalDate blacklistedOn) {
		this.blacklistedOn = blacklistedOn;
	}
}
