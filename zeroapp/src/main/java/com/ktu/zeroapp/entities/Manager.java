package com.ktu.zeroapp.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.*;

@Entity
public class Manager extends Employee{
	@Id
	@GeneratedValue
	private Long id;
	@Column
	private String name;

	public Manager() {
	}

	public Manager(Employee employee, String name) {
		this.setFirstName(employee.getFirstName());
		this.setLastName(employee.getLastName());
		this.setPersonalIdentificationNumber(employee.getPersonalIdentificationNumber());
		this.setBankAccount(employee.getBankAccount());
		this.setAddress(employee.getAddress());
		this.setPhoneNumber(employee.getPhoneNumber());
		this.setCondition(employee.getCondition());
		this.setSalary(employee.getSalary());
		this.setHoliday(employee.getHoliday());
		this.setStartedWork(employee.getStartedWork());
		this.setEmail(employee.getEmail());
		this.setPosition(employee.getPosition());
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
