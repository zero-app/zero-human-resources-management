package com.ktu.zeroapp.services;

/**
 * Created by Aivaras on 31/05/2017.
 */
public interface ServiceAdapter {
    boolean arePropertiesSet(Object o);
}
