package com.ktu.zeroapp.services;

import org.springframework.stereotype.Service;

/**
 * Created by Aivaras on 31/05/2017.
 */
@Service
public class NullService implements ServiceAdapter {
    @Override
    public boolean arePropertiesSet(Object o) {
        return false;
    }
}
