package com.ktu.zeroapp.services;

import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.RegistrationCode;
import com.ktu.zeroapp.entities.User;
import com.ktu.zeroapp.utils.AuthenticationUtil;
import com.ktu.zeroapp.utils.EmployeeCondition;
import com.ktu.zeroapp.utils.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DataPreloadingService {
    @Autowired
    UserService userService;

    @Autowired
    EmployeeServices employeeServices;

    @Autowired
    AuthenticationUtil authenticationUtil;

    public void loadTestData(){

        Employee employee = saveEmployee("John","Snow");
        saveEmployee("no1","no1ln");
        saveEmployee("no2","no2ln");
        saveEmployee("no3","no3ln");
        saveEmployee("no4","no4ln");
        saveEmployee("no5","no5ln");

        saveEmployeeWithRegistrationCode("code", "code");

        saveUser(UserRole.ADMINISTRATOR,"admin", "admin",employee);
    }

    private Employee saveEmployee(String name, String surname){
        Employee employee = new Employee(name,surname,12345678912L,
                "LT000000000000000","Kreves g. 111 - 11, Kaunas","+37060000000",
                EmployeeCondition.ACTIVE.getName(),200.0,20.0, LocalDate.now(), "test@test.com", "employee");
        employee = employeeServices.saveEmployee(employee);

        return employee;
    }

    private Employee saveEmployeeWithRegistrationCode(String name, String surname)
    {
        Employee savedEmployee = saveEmployee(name, surname);
        userService.saveRegistrationCode(new RegistrationCode("0123456789", savedEmployee.getId()));

        return savedEmployee;
    }
    private User saveUser(UserRole role,String username, String password, Employee employee){
        User user = new User();
        user.setRole(role.getRoleValue());
        user.setUsername(username);
        user.setPassword(password);
        user.setEmployeeId(employee.getId());
        userService.saveNewUser(user);
        return user;
    }
}
