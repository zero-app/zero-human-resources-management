package com.ktu.zeroapp.services;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.RegistrationCode;
import com.ktu.zeroapp.repositories.IRegistrationCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.ktu.zeroapp.entities.User;
import com.ktu.zeroapp.repositories.IUserRepository;
import com.ktu.zeroapp.utils.AuthenticationUtil;
import com.ktu.zeroapp.utils.ResponseCode;

@Service
public class UserService {

	@Autowired
	IUserRepository userRepository;

	@Autowired
    IRegistrationCodeRepository registrationCodeRepository;

	@Autowired
	AuthenticationUtil authenticationUtil;

	public ResponseCode validateUser(String username, String password) {
		List<User> list = userRepository.findUserByUsername(username);
		if (CollectionUtils.isEmpty(list)){
			return ResponseCode.USER_DOES_NOT_EXIST;
		}
		User user = list.get(0);
		return authenticationUtil.validate(user.getId(),password) ? ResponseCode.SUCCESSFUL_LOGIN : ResponseCode.INCORRECT_PASSWORD;
	}

	public void saveNewUser(User user){
			user.setPassword(authenticationUtil.encrypt(user.getPassword()));
			userRepository.save(user);

	}

	public User getUser(String username)
	{
		List<User> list = userRepository.findUserByUsername(username);
		if(CollectionUtils.isEmpty(list))
			return null;
		return list.get(0);
	}

	public User getUser(Long employeeId)
	{
		return userRepository.findUserByEmployeeId(employeeId);
	}

	public void deleteUser(User user)
	{
		userRepository.delete(user);
	}

	public RegistrationCode saveRegistrationCode(RegistrationCode registrationCode)
	{
		return registrationCodeRepository.save(registrationCode);
	}

	public RegistrationCode getRegistrationCode(Employee employee)
	{
		return registrationCodeRepository.getRegistrationCodeByEmployeeId(employee.getId());
	}

	public RegistrationCode getRegistrationCode(String code)
	{
		return registrationCodeRepository.getRegistrationCodeByCode(code);
	}

	public void deleteRegistrationCode(RegistrationCode registrationCode)
	{
		registrationCodeRepository.delete(registrationCode);
	}
}
