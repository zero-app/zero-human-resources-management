package com.ktu.zeroapp.services;


import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.HolidayRequest;
import com.ktu.zeroapp.repositories.IHolidayRequestRepository;
import com.ktu.zeroapp.utils.HolidayStatus;
import com.ktu.zeroapp.viewModels.HolidayRequestViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestServices implements ServiceAdapter{
    @Autowired
    private IHolidayRequestRepository holidayRequestRepository;

    @Autowired
    private EmployeeServices employeeServices;

    public HolidayRequest submitHolidayRequest(HolidayRequest holidayRequest) {
        return holidayRequestRepository.save(holidayRequest);
    }

    public List<HolidayRequestViewModel> getHolidayRequestList() {
        List<HolidayRequestViewModel> list = new ArrayList<>();
        List<HolidayRequest> holidayRequests = holidayRequestRepository.findAll();
        for (HolidayRequest request : holidayRequests) {

            Employee e = employeeServices.getEmployee(request.getEmployeeId());
            String fullName = e.getFirstName() + " " + e.getLastName();

            list.add(new HolidayRequestViewModel(request.getId(), fullName, request.getStart().toString(), request.getEnd().toString(),
                    request.getComment(), request.getCondition()));
        }

        return list;
    }

    public HolidayRequest acceptHolidayRequest(Long id) {
        HolidayRequest holidayRequest = holidayRequestRepository.findOne(id);
        holidayRequest.setCondition(HolidayStatus.APPROVED.toString());
        return holidayRequestRepository.save(holidayRequest);
    }

    public HolidayRequest declineHolidayRequest(Long id) {
        HolidayRequest holidayRequest = holidayRequestRepository.findOne(id);
        holidayRequest.setCondition(HolidayStatus.DECLINED.toString());
        return holidayRequestRepository.save(holidayRequest);
    }

    @Override
    public boolean arePropertiesSet(Object o) {
        HolidayRequest request = (HolidayRequest) o;
        return request.getStart() != null && request.getEnd() != null && !StringUtils.isEmpty(request.getCondition()) && request.getEmployeeId() != null;
    }
}
