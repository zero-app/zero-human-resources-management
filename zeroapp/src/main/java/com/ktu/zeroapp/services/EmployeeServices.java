package com.ktu.zeroapp.services;

import com.ktu.zeroapp.entities.BlacklistedEmployee;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.repositories.IBlacklistedEmployeeRepository;
import com.ktu.zeroapp.repositories.IEmployeeRepository;
import com.ktu.zeroapp.utils.EmployeeCondition;
import com.ktu.zeroapp.viewModels.BlacklistedEmployeeViewModel;
import com.ktu.zeroapp.viewModels.EmployeeViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeServices implements ServiceAdapter{

	@Autowired
	IEmployeeRepository employeeRepository;

	@Autowired
	IBlacklistedEmployeeRepository blacklistedEmployeeRepository;

	public List<EmployeeViewModel> getEmployeeList() {
		List<Employee> list =  employeeRepository.findAll();
		List<EmployeeViewModel> models = new ArrayList<>();
		list.forEach(employee -> {
			EmployeeViewModel model = new EmployeeViewModel(employee);
			models.add(model);
		});
		return models;
	}

	/**
	 * Find all active employees.
	 * @return list of active employee view models.
	 */
	public List<EmployeeViewModel> getActiveEmployees(){
		List<Employee> employees = employeeRepository.findAllByCondition(EmployeeCondition.ACTIVE.getName());
		List<EmployeeViewModel> models = new ArrayList<>();
		employees.forEach(employee -> {
			EmployeeViewModel model = new EmployeeViewModel(employee);
			models.add(model);
		});
		return models;
	}

	public Employee getEmployee(Long id) {
		Employee e = employeeRepository.findOne(id);
		return e;
	}

	public Employee saveEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	/**
	 * Add new employee to blacklist employee repository and modify condition to blacklisted.
	 * @param employee - employee to blacklist.
	 * @param comment - blacklisting reason or comment.
	 */
	public void blacklistEmployee(Employee employee, String comment) {

		BlacklistedEmployee blacklistedEmployee = new BlacklistedEmployee(LocalDate.now(),comment, employee.getId());
		BlacklistedEmployee saved = blacklistedEmployeeRepository.save(blacklistedEmployee);
		Employee employeeToBlacklist = employeeRepository.findOne(employee.getId());
		employeeToBlacklist.setCondition(EmployeeCondition.BLACKLISTED.getName());
		employeeToBlacklist.setBlacklistedEmployeeId(saved.getId());
		employeeRepository.save(employeeToBlacklist);
	}

	/**
	 * Get all blacklisted employee data.
	 * @return list of blacklisted employee view models.
	 */
	public List<BlacklistedEmployeeViewModel> getAllBlacklistedEmployeeData(){
		List<BlacklistedEmployee> blacklistedEmployees = blacklistedEmployeeRepository.findAll();
		List<BlacklistedEmployeeViewModel> models = new ArrayList<>();
		if(!CollectionUtils.isEmpty(blacklistedEmployees)){
			List<Long> employeeIds = new ArrayList<>();
			for(BlacklistedEmployee b: blacklistedEmployees){
				employeeIds.add(b.getEmployeeId());
			}
			List<Employee> employees = employeeRepository.findAllByIds(employeeIds);
			for(BlacklistedEmployee b : blacklistedEmployees){
				for(Employee e : employees){
					if(e.getId() == b.getEmployeeId()){
						models.add(new BlacklistedEmployeeViewModel(b.getId(),e.getFirstName(),e.getLastName(), e.getStartedWork(), b.getBlacklistedOn(), b.getComment()));
					}
				}
			}
		}
		return models;
	}

	@Override
	public boolean arePropertiesSet(Object o) {
		Employee e = (Employee) o;
		return !StringUtils.isEmpty(e.getFirstName()) && !StringUtils.isEmpty(e.getLastName()) && !StringUtils.isEmpty(e.getAddress()) && !StringUtils.isEmpty(e.getEmail());
	}
}
