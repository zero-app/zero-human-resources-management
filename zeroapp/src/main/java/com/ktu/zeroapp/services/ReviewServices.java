package com.ktu.zeroapp.services;

import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.Manager;
import com.ktu.zeroapp.entities.Review;
import com.ktu.zeroapp.repositories.IManagerRepository;
import com.ktu.zeroapp.repositories.IReviewRepository;
import com.ktu.zeroapp.viewModels.ReviewViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
public class ReviewServices implements ServiceAdapter{

    @Autowired
    private IManagerRepository managerRepository;

    @Autowired
    private IReviewRepository reviewRepository;

    @Autowired
    private EmployeeServices employeeServices;

    public void saveNewReview(Review review) {
//        Employee employee = employeeServices.getEmployee(review.getEmployeeId());
//        Manager m = new Manager(employee,"");
//        managerRepository.save(m);
//        review.setManagerId(m.getId());
        reviewRepository.save(review);
    }

    public ArrayList<ReviewViewModel> getReviews() {
        ArrayList<Review> list = (ArrayList<Review>) reviewRepository.findAll();
        ArrayList<ReviewViewModel> models = new ArrayList<>();
        for (Review r : list) {
            Employee employee = employeeServices.getEmployee(r.getEmployeeId());
            String name = employee.getFirstName() + " " + employee.getLastName();
            models.add(new ReviewViewModel(r, name));
        }
        return models;
    }

    /**
     * Find all reviews made within date range.
     *
     * @param from - start of date range.
     * @param to   - end of date range.
     * @return list of reviews.
     */
    public ArrayList<ReviewViewModel> getReviewsWithinDateRange(LocalDate from, LocalDate to) {
        ArrayList<Review> reviews = (ArrayList<Review>) reviewRepository.findAllByDateOfReviewWithinDateRange(from, to);
        ArrayList<ReviewViewModel> models = new ArrayList<>();
        reviews.forEach(review -> {
            Employee e = employeeServices.getEmployee(review.getEmployeeId());
            models.add(new ReviewViewModel(review, e.getFirstName() + " " + e.getLastName()));
        });
        return models;
    }

    public void removeReview(ReviewViewModel review) {
        reviewRepository.delete(review.getId());
    }

    public Review getReview(long id) {
        return reviewRepository.findOne(id);
    }

    @Override
    public boolean arePropertiesSet(Object o) {
        Review review = (Review) o;
        return review.getBegining()!=null && review.getEnd()!=null && review.getGrade() != null && review.getImprovement() != null && review.getQuality() != null;
    }
}
