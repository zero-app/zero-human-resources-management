package com.ktu.zeroapp;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ktu.zeroapp.services.DataPreloadingService;
import com.ktu.zeroapp.utils.Loader;
import javafx.application.HostServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

@SpringBootApplication
public class Main extends Application {

	@Autowired
	DataPreloadingService dataPreloadingService;
	private static Stage stage;

	private static ApplicationContext ctx;

	private static DoubleProperty widthProperty;
	private static DoubleProperty heightProperty;
	private static HostServices hostServices;

	public static DoubleProperty getWidthProperty(){
		return widthProperty;
	}
	public static DoubleProperty getHeightProperty(){
		return heightProperty;
	}
	public static HostServices getHost()
	{
		return hostServices;
	}

	private static BorderPane root = new BorderPane();

	public static BorderPane getRoot() {
		return root;
	}

	public static Stage getPrimaryStage() {
		return stage;
	}

	private final Loader loader = new Loader();

	@Override
	public void start(Stage primaryStage) {
		try {
			stage = primaryStage;
			stage.setTitle("ZERO");


			stage.setResizable(false);
			Scene scene = new Scene(root,1000,500);
			stage.setScene(scene);
			stage.getIcons().add(new Image("/images/z-logo01.png"));

			gotoLogin();
			widthProperty = new SimpleDoubleProperty();
			widthProperty.bind(scene.widthProperty());
			heightProperty = new SimpleDoubleProperty();
			heightProperty.bind(scene.heightProperty());
			hostServices = getHostServices();
			root.prefWidthProperty().bind(widthProperty);
			root.prefHeightProperty().bind(heightProperty);
			primaryStage.show();

			Main.getContext().getBean(DataPreloadingService.class).loadTestData();

		} catch (Exception ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void gotoLogin()
	{
		loader.goTo(Loader.LOGIN_WINDOW_URL);
	}

	public static ApplicationContext getContext(){
		return ctx;
	}

	public static void main(String[] args) {
		ctx = SpringApplication.run(Main.class,args);

		launch(args);
	}
}
