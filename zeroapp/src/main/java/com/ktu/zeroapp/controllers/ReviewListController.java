package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.services.ReviewServices;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.Loader;
import com.ktu.zeroapp.viewModels.ReviewViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URL;
import java.util.ResourceBundle;

public class ReviewListController implements Initializable
{
    private Loader loader = new Loader();
    private final ReviewServices reviewServices = Main.getContext().getBean(ReviewServices.class);
    @Autowired
    private EmployeeServices employeeServices;
    @FXML
    public DatePicker dateFrom;
    @FXML
    public DatePicker dateTo;
    @FXML
    public Button showButton;
    @FXML
    public Button modifyButton;
    @FXML
    public Button deleteButton;

    @FXML
    private TableView<ReviewViewModel> reviewTable;
    @FXML
    private TableColumn<ReviewViewModel, String> nameColumn;
    @FXML
    private TableColumn<ReviewViewModel, String> dateColumn;
    @FXML
    private TableColumn<ReviewViewModel, Integer> qualityColumn;
    @FXML
    private TableColumn<ReviewViewModel, Integer> gradeColumn;
    @FXML
    private TableColumn<ReviewViewModel, Integer> improvementColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        dateColumn.setCellValueFactory(cellData -> cellData.getValue().dateStringPropertyProperty());
        qualityColumn.setCellValueFactory(cellData -> cellData.getValue().qualityPropertyProperty().asObject());
        gradeColumn.setCellValueFactory(cellData -> cellData.getValue().gradePropertyProperty().asObject());
        improvementColumn.setCellValueFactory(cellData -> cellData.getValue().improvementPropertyProperty().asObject());

        final ObservableList<ReviewViewModel> models = FXCollections.observableArrayList(reviewServices.getReviews());
        reviewTable.setItems(models);
    }

    public void openModifyReviewWindow(ActionEvent actionEvent) {
        ReviewViewModel selectedReview = reviewTable.getSelectionModel().getSelectedItem();
        if(selectedReview == null){
            AlertUtil.displayAlert(Alert.AlertType.ERROR,"You must select review to be modified");
            return;
        }
        ReviewController controller = loader.goTo(Loader.REVIEW_WINDOW_URL).getController();
        controller.editReview(selectedReview);
    }

    /**
     * Remove review from table and delete from database.
     * @param actionEvent
     */
    public void openDeleteConfirmation(ActionEvent actionEvent) {
        ReviewViewModel review =reviewTable.getSelectionModel().getSelectedItem();
        boolean remove = AlertUtil.confirmationAlert("Delete confirmation","Do you want to remove review?", "Reviewed employee - "+review.getName());

        if(remove && review !=null){
            reviewServices.removeReview(review);
        }
        reviewTable.getItems().remove(review);
    }

    public void showReviewList(ActionEvent actionEvent) {
        if(dateFrom.getValue() != null && dateTo.getValue() != null && dateFrom.getValue().isBefore(dateTo.getValue())) {
            final ObservableList<ReviewViewModel> models = FXCollections.observableArrayList(reviewServices.getReviewsWithinDateRange(dateFrom.getValue(), dateTo.getValue()));
            reviewTable.setItems(models);
        }else {
            AlertUtil.displayAlert(Alert.AlertType.ERROR,"Error","Wrong date input","Please confirm date range entered, both dates have to be entered and start date have to be before end date..");
        }
    }

    public void cancel(ActionEvent actionEvent) {

    }
}
