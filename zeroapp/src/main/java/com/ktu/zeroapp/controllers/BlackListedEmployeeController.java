package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.viewModels.BlacklistedEmployeeViewModel;
import com.ktu.zeroapp.viewModels.EmployeeViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import org.springframework.util.CollectionUtils;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class BlackListedEmployeeController implements Initializable
{
    private final EmployeeServices employeeServices = Main.getContext().getBean(EmployeeServices.class);
    @FXML
    public TableView<BlacklistedEmployeeViewModel> blackListedEmployeeTable;
    @FXML
    private TableColumn<BlacklistedEmployeeViewModel, String> firstNameColumn;
    @FXML
    private TableColumn<BlacklistedEmployeeViewModel, String> lastNameColumn;
    @FXML
    private TableColumn<BlacklistedEmployeeViewModel, String> startedEmploymentColumn;
    @FXML
    private TableColumn<BlacklistedEmployeeViewModel, String> endedEmploymentColumn;
    @FXML
    private TableColumn<BlacklistedEmployeeViewModel, String> addedToBlacklistOnColumn;

    @FXML
    private TextArea commentArea;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        startedEmploymentColumn.setCellValueFactory(cellData -> cellData.getValue().startedEmploymentProperty());
        endedEmploymentColumn.setCellValueFactory(cellData -> cellData.getValue().endedEmploymentProperty());
        addedToBlacklistOnColumn.setCellValueFactory(cellData -> cellData.getValue().addedToBlacklistOnProperty());

        List<BlacklistedEmployeeViewModel> blacklistedEmployeeViewModels = employeeServices.getAllBlacklistedEmployeeData();
        if(CollectionUtils.isEmpty(blacklistedEmployeeViewModels)){
            return;
        }

        final ObservableList<BlacklistedEmployeeViewModel> models = FXCollections.observableArrayList(blacklistedEmployeeViewModels);
        blackListedEmployeeTable.setItems(models);
        blackListedEmployeeTable.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            if(newValue!=null){
                commentArea.setText(newValue.getComment());
            }
        }));
    }
}
