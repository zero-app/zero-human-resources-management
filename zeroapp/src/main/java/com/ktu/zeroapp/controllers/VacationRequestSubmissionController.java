package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.HolidayRequest;
import com.ktu.zeroapp.services.RequestServices;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.AuthenticationUtil;
import com.ktu.zeroapp.utils.HolidayStatus;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.time.LocalDate;

/**
 * Provides functionality if there is a necessity issuing
 * vacation requests.
 *
 * @author Sarunas Sarakojis
 */
public class VacationRequestSubmissionController {

    public TextField vacationRequestOwner;
    public DatePicker vacationStartDate;
    public DatePicker vacationEndDate;

    public void initialize() {
        LocalDate currentDate = LocalDate.now();

        vacationRequestOwner.setText(AuthenticationUtil.getLoggedUser().getUsername());
        vacationStartDate.setValue(currentDate);
        vacationEndDate.setValue(currentDate);
    }

    public void submitVacationRequest() {

        if (isInputDataValid()) {
            if (submitVacationRequestToRepository() != null) {
                displayInformationAlert("Success", "Vacation request was successfully submitted");
            } else {
                displayErrorAlert("Failure", "Vacation request was not submitted");
            }
        } else {
            displayErrorAlert("Error", "Incorrect input");
        }
    }

    private boolean isInputDataValid() {
        LocalDate vacationStart = vacationStartDate.getValue();
        LocalDate vacationEnd = vacationEndDate.getValue();

        if (vacationStart == null || vacationEnd == null) {
            displayErrorAlert("No input", "");
            return false;
        }

        return true;
    }

    private HolidayRequest submitVacationRequestToRepository() {
        RequestServices requestServices = Main.getContext().getBean(RequestServices.class);
        LocalDate vacationStart = vacationStartDate.getValue();
        LocalDate vacationEnd = vacationEndDate.getValue();

        return requestServices.submitHolidayRequest(new HolidayRequest(vacationStart, vacationEnd, "",
                HolidayStatus.WAITING.toString(), AuthenticationUtil.getEmployeeId()));
    }

    private void displayErrorAlert(String title, String header) {
        AlertUtil.displayAlert(Alert.AlertType.ERROR, title, header);
    }

    private void displayInformationAlert(String title, String header) {
        AlertUtil.displayAlert(Alert.AlertType.INFORMATION, title, header);
    }
}
