package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.Position;
import com.ktu.zeroapp.entities.RegistrationCode;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.services.UserService;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.EmployeeCondition;
import com.ktu.zeroapp.utils.Loader;
import com.ktu.zeroapp.utils.RegexUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AddEmployeeController
{
    private final Loader loader = new Loader();
    private final UserService userService = Main.getContext().getBean(UserService.class);
    private final EmployeeServices employeeServices = Main.getContext().getBean(EmployeeServices.class);;

    private Employee employee = new Employee();
    private boolean isNew = true;

    private StringBuilder notificationText = new StringBuilder();

    @FXML
    private TextField nameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField citizenCodeField;
    @FXML
    private TextField phoneField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField addressField;
    @FXML
    private TextField bankAccountField;
    @FXML
    private TextField monthlySalaryField;
    @FXML
    private TextField holidayDayCountField;

    @FXML
    private ComboBox<String> salaryConditionBox;

    @FXML
    public ComboBox<String> positionComboBox;

    @FXML
    private void initialize()
    {
        List<String> conditionList = new ArrayList<>();
        conditionList.add(EmployeeCondition.ACTIVE.getName());
        conditionList.add(EmployeeCondition.INACTIVE.getName());
        conditionList.add(EmployeeCondition.SUSPENDED.getName());

        salaryConditionBox.getItems().addAll(conditionList);

        List<String> positionList = new ArrayList<>();
        for(Position p : Position.values()){
            positionList.add(p.getName());
        }
        positionComboBox.getItems().addAll(positionList);
    }

    public void initialize(Employee employee)
    {
        initialize();

        if(employee != null)
        {
            this.employee = employee;
            isNew = false;
        }
        else
        {
            return;
        }

        nameField.setText(employee.getFirstName());
        lastNameField.setText(employee.getLastName());
        citizenCodeField.setText(employee.getPersonalIdentificationNumber().toString());
        phoneField.setText(employee.getPhoneNumber());
        emailField.setText(employee.getEmail());
        addressField.setText(employee.getAddress());
        bankAccountField.setText(employee.getBankAccount());
        monthlySalaryField.setText(employee.getSalary().toString());
        holidayDayCountField.setText(employee.getHoliday().toString());
        salaryConditionBox.getSelectionModel().select(employee.getCondition());
        positionComboBox.getSelectionModel().select(employee.getPosition());
    }

    private void buildNotification(String s)
    {
        notificationText.append(s);
        notificationText.append("\n");
    }

    @FXML
    private void execute()
    {
        if (validateFields())
        {
            employee.setFirstName(nameField.getText());
            employee.setLastName(lastNameField.getText());
            employee.setPersonalIdentificationNumber(Long.valueOf(citizenCodeField.getText()));
            employee.setBankAccount(bankAccountField.getText());
            employee.setAddress(addressField.getText());
            employee.setPhoneNumber(phoneField.getText());
            employee.setCondition(salaryConditionBox.getSelectionModel().getSelectedItem());
            employee.setSalary(Double.valueOf(monthlySalaryField.getText()));
            employee.setHoliday(Double.valueOf(holidayDayCountField.getText()));
            employee.setEmail(emailField.getText());
            employee.setPosition(positionComboBox.getSelectionModel().getSelectedItem());

            if(isNew)
            {
                employee.setStartedWork(LocalDate.now());
            }

            Employee savedEmployee = employeeServices.saveEmployee(employee);

            if(isNew)
            {
                RegistrationCode registrationCode = generateRegistrationCode(savedEmployee);

                while(userService.getRegistrationCode(registrationCode.getCode()) != null)
                {
                    registrationCode = generateRegistrationCode(savedEmployee);
                }

                userService.saveRegistrationCode(registrationCode);
            }

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Operation was successful!");
            loader.goTo(Loader.EMPLOYEE_LIST_WINDOW_URL);
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING,"Input error","There was an error with your input",notificationText.toString());
        }
    }

    private boolean validateFields()
    {
        notificationText.setLength(0);
        boolean isCorrect = true;
        if(positionComboBox.getSelectionModel().getSelectedItem() == null || positionComboBox.getSelectionModel().getSelectedItem().trim().isEmpty())
        {
            buildNotification("Position is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfOnlyLetters(nameField.getText()))
        {
            buildNotification("First name is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfOnlyLetters(lastNameField.getText()))
        {
            buildNotification("Last name is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfOnlyNumbers(citizenCodeField.getText()))
        {
            buildNotification("Personal identification number is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfAlphaNumeric(bankAccountField.getText()))
        {
            buildNotification("Bank account number is incorrect");
            isCorrect = false;
        }
        if(addressField.getText() == null || addressField.getText().trim().isEmpty())
        {
            buildNotification("Address is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfPhoneNumber(phoneField.getText()))
        {
            buildNotification("Phone number is incorrect");
            isCorrect = false;
        }
        if(salaryConditionBox.getSelectionModel().getSelectedItem() == null || salaryConditionBox.getSelectionModel().getSelectedItem().trim().isEmpty())
        {
            buildNotification("Condition is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfOnlyNumbers(monthlySalaryField.getText()))
        {
            buildNotification("Salary is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfOnlyNumbers(holidayDayCountField.getText()))
        {
            buildNotification("Holiday is incorrect");
            isCorrect = false;
        }
        if(!RegexUtil.checkIfEmail(emailField.getText()))
        {
            buildNotification("E-mail is incorrect");
            isCorrect = false;
        }

        return isCorrect;
    }

    private RegistrationCode generateRegistrationCode(Employee employee)
    {
        String availableCharacters = "qwertyuiopasdfghjklzxcvbnm0123456789";

        StringBuilder code = new StringBuilder();
        Random random = new Random();
        for(int i = 0; i < 10; i++)
        {
            int charPlace = random.nextInt(availableCharacters.length() + 1);
            code.append(availableCharacters.charAt(charPlace));
        }

        return new RegistrationCode(code.toString(), employee.getId());
    }
}
