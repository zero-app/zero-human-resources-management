package com.ktu.zeroapp.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class WelcomeController implements Initializable
{
    @FXML
    private Text welcomeText;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        welcomeText.setText("Welcome, today is: " + dateFormat.format(new Date()));
    }
}
