package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.services.RequestServices;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.HolidayStatus;
import com.ktu.zeroapp.viewModels.HolidayRequestViewModel;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.List;
import java.util.function.Function;

/**
 * @author Sarunas Sarakojis
 */
public class ManageSubmittedVacationRequestsController {

    public TableView<HolidayRequestViewModel> vacationRequestsTable;
    public TableColumn<HolidayRequestViewModel, String> vacationRequestOwnerColumn;
    public TableColumn<HolidayRequestViewModel, String> vacationStartColumn;
    public TableColumn<HolidayRequestViewModel, String> vacationEndColumn;
    public TableColumn<HolidayRequestViewModel, String> vacationRequestStatusColumn;

    public void initialize() {
        vacationRequestOwnerColumn.setCellValueFactory(feature -> feature.getValue().fullNameProperty());
        vacationStartColumn.setCellValueFactory(feature -> feature.getValue().vacationStartDateProperty());
        vacationEndColumn.setCellValueFactory(feature -> feature.getValue().vacationEndDateProperty());
        vacationRequestStatusColumn.setCellValueFactory(feature -> feature.getValue().statusProperty());

        fillTableWithVacationRequests();
    }

    private void fillTableWithVacationRequests() {
        List<HolidayRequestViewModel> holidayRequestList = Main.getContext().getBean(RequestServices.class).getHolidayRequestList();

        vacationRequestsTable.setItems(FXCollections.observableArrayList(holidayRequestList));
    }

    public void acceptSelectedRequest() {
        HolidayRequestViewModel selectedRequest = vacationRequestsTable.getSelectionModel().getSelectedItem();

        if (selectedRequest != null && isVacationRequestAccepted(selectedRequest.getId())) {
            displayInformationAlert("Success", "Vacation request accepted");
            fillTableWithVacationRequests();
        } else {
            displayErrorAlert("Not selected", "No vacation request selected");
        }
    }

    public void declineSelectedRequest() {
        HolidayRequestViewModel selectedRequest = vacationRequestsTable.getSelectionModel().getSelectedItem();

        if (selectedRequest != null && isVacationRequestDeclined(selectedRequest.getId())) {
            displayInformationAlert("Success", "Vacation request declined");
            fillTableWithVacationRequests();
        } else {
            displayErrorAlert("Not selected", "No vacation request selected");
        }
    }

    private static boolean isVacationRequestAccepted(Long requestID) {
        return applyActionWithRequestServices(service ->
                service.acceptHolidayRequest(requestID).getCondition().equals(HolidayStatus.APPROVED.toString()));
    }

    private static boolean isVacationRequestDeclined(Long requestID) {
        return applyActionWithRequestServices(service ->
                service.declineHolidayRequest(requestID).getCondition().equals(HolidayStatus.DECLINED.toString()));
    }

    private static <T> T applyActionWithRequestServices(Function<RequestServices, T> stuff) {
        return stuff.apply(Main.getContext().getBean(RequestServices.class));
    }

    private void displayErrorAlert(String title, String header) {
        AlertUtil.displayAlert(Alert.AlertType.ERROR, title, header);
    }

    private void displayInformationAlert(String title, String header) {
        AlertUtil.displayAlert(Alert.AlertType.INFORMATION, title, header);
    }
}
