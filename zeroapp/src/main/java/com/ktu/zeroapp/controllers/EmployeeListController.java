package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.RegistrationCode;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.services.UserService;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.Loader;
import com.ktu.zeroapp.viewModels.EmployeeViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

public class EmployeeListController implements Initializable
{
    private final Loader loader = new Loader();
    private final EmployeeServices employeeServices = Main.getContext().getBean(EmployeeServices.class);
    private final UserService userService = Main.getContext().getBean(UserService.class);

    @FXML
    private TableView<EmployeeViewModel> employeeTable;
    @FXML
    private TableColumn<EmployeeViewModel, String> firstNameColumn;
    @FXML
    private TableColumn<EmployeeViewModel, String> lastNameColumn;
    @FXML
    private TableColumn<EmployeeViewModel, String> phoneColumn;
    @FXML
    private TableColumn<EmployeeViewModel, String> addressColumn;
    @FXML
    private TableColumn<EmployeeViewModel, String> dateStartedColumn;
    @FXML
    private TableColumn<EmployeeViewModel, Double> salaryColumn;
    @FXML
    private TableColumn<EmployeeViewModel, Double> holidayColumn;
    @FXML
    private TableColumn<EmployeeViewModel, String> emailColumn;

    @FXML
    private ComboBox<String> actionBox;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        firstNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        lastNameColumn.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        phoneColumn.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        addressColumn.setCellValueFactory(cellData -> cellData.getValue().addressProperty());
        dateStartedColumn.setCellValueFactory(cellData -> cellData.getValue().dateStartedProperty());
        salaryColumn.setCellValueFactory(cellData -> cellData.getValue().salaryProperty().asObject());
        holidayColumn.setCellValueFactory(cellData -> cellData.getValue().holidayProperty().asObject());
        emailColumn.setCellValueFactory(cellData -> cellData.getValue().emailProperty());

        final ObservableList<EmployeeViewModel> models = FXCollections.observableArrayList(employeeServices.getActiveEmployees());
        employeeTable.setItems(models);
    }

    @FXML
    private void executeAction()
    {
        String option = actionBox.getSelectionModel().getSelectedItem();

        if(option == null)
            return;

        switch (option){
            case ("Add new"):
                loader.goTo(Loader.ADD_EMPLOYEE_WINDOW_URL);

                break;
            case("Modify"):
                if(employeeTable.getSelectionModel().getSelectedItem() != null)
                {
                    AddEmployeeController controller = loader.goTo(Loader.ADD_EMPLOYEE_WINDOW_URL).getController();
                    controller.initialize(employeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId()));
                }
                else
                    AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing was selected");

                break;
            case("Remove"):
                if(employeeTable.getSelectionModel().getSelectedItem() != null)
                {
                    RemoveEmployeeController controller = loader.goTo(Loader.REMOVE_EMPLOYEE_WINDOW_URL).getController();
                    controller.initialize(employeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId()));
                }
                else
                    AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing was selected");

                break;
            case("Review"):
                if(employeeTable.getSelectionModel().getSelectedItem() != null)
                {
                    ReviewController controller = loader.goTo(Loader.REVIEW_WINDOW_URL).getController();
                    controller.initialize(employeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId()));
                }
                else
                    AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing was selected");

                break;
            case("Send registration code"):
                sendRegistrationCode();

                break;
            default:
                break;
        }
    }

    private void sendRegistrationCode()
    {
        if(employeeTable.getSelectionModel().getSelectedItem() != null)
        {
            Employee employee = employeeServices.getEmployee(employeeTable.getSelectionModel().getSelectedItem().getId());
            RegistrationCode registrationCode = userService.getRegistrationCode(employee);

            if(registrationCode != null)
                Main.getHost().showDocument("mailto:" + employee.getEmail() + "?subject=Your%20registration%20code&&body=" + registrationCode.getCode());
            else
                AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Employee does not have registration code");
        }
        else
            AlertUtil.displayAlert(Alert.AlertType.WARNING, "Warning", "Nothing was selected");
    }
}
