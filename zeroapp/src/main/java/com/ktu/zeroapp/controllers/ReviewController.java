package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.Review;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.services.ReviewServices;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.AuthenticationUtil;
import com.ktu.zeroapp.utils.Loader;
import com.ktu.zeroapp.viewModels.ReviewViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Map;

public class ReviewController
{
    private Employee employee;
    private Review review;

    private final Loader loader = new Loader();
    private final ReviewServices reviewServices = Main.getContext().getBean(ReviewServices.class);
    private final EmployeeServices employeeServices = Main.getContext().getBean(EmployeeServices.class);

    @FXML
    private TextField nameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private DatePicker dateFromPicker;
    @FXML
    private DatePicker dateToPicker;
    @FXML
    private ComboBox<Integer> improvementBox;
    @FXML
    private ComboBox<Integer> qualityBox;
    @FXML
    private ComboBox<Integer> overallGradeBox;
    @FXML
    private TextArea commentArea;


    public void initialize(Employee employee)
    {
        this.employee = employee;

        nameField.setText(employee.getFirstName());
        lastNameField.setText(employee.getLastName());
    }

    public void editReview(ReviewViewModel reviewViewModel){
        this.review = reviewServices.getReview(reviewViewModel.getId());
        initialize(employeeServices.getEmployee(review.getEmployeeId()));
        this.dateFromPicker.setValue(review.getBegining());
        this.dateToPicker.setValue(review.getEnd());
        this.improvementBox.getSelectionModel().select(review.getImprovement());
        this.overallGradeBox.getSelectionModel().select(review.getGrade());
        this.qualityBox.getSelectionModel().select(review.getQuality());
        this.commentArea.setText(review.getComment());

    }

    @FXML
    private void execute()
    {
        if(this.review == null || this.review.getId() == null) {
            this.review = new Review();
        }
        review.setEmployeeId(employee.getId());
        review.setManagerId(AuthenticationUtil.getEmployeeId());
        review.setComment(commentArea.getText());
        review.setDateOfReview(LocalDate.now());
        review.setBegining(dateFromPicker.getValue());
        review.setEnd(dateToPicker.getValue());
        review.setGrade(overallGradeBox.getSelectionModel().getSelectedItem());
        review.setImprovement(improvementBox.getSelectionModel().getSelectedItem());
        review.setQuality(qualityBox.getSelectionModel().getSelectedItem());
        reviewServices.saveNewReview(review);
        if(!reviewServices.arePropertiesSet(review)){
            AlertUtil.displayAlert(Alert.AlertType.ERROR,"Please fill in all the fields.");
            return;
        }

        loader.goTo(Loader.EMPLOYEE_LIST_WINDOW_URL);
    }

    @FXML
    private void cancel()
    {
        loader.goTo(Loader.REVIEW_LIST_WINDOW_URL);
    }
}
