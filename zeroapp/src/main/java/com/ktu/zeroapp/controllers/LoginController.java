package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.services.UserService;
import com.ktu.zeroapp.utils.*;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.springframework.util.StringUtils;

public class LoginController
{
    private Loader loader = new Loader();
    private UserService userService = Main.getContext().getBean(UserService.class);

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;

    @FXML
    private void login()
    {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if(validateFields())
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Please enter the username/password");
            return;
        }

        ResponseCode response = userService.validateUser(username,password);

        if(response == ResponseCode.INCORRECT_PASSWORD || response == ResponseCode.USER_DOES_NOT_EXIST)
        {
            AlertUtil.displayAlert(Alert.AlertType.ERROR, "Error", "Incorrect password or user does not exist");
            return;
        }

        if(response == ResponseCode.SUCCESSFUL_LOGIN)
        {
            loader.goTo(Loader.WELCOME_WINDOW_URL);
            if(AuthenticationUtil.getLoggedUser().getRole().equals(UserRole.ADMINISTRATOR.getRoleValue()))
                loader.loadMenu(Loader.ADMIN_MENU_URL);
            else
                loader.loadMenu(Loader.EMPLOYEE_MENU_URL);
        }
    }

    @FXML
    private void toRegister()
    {
        loader.goTo(Loader.REGISTRATION_WINDOW_URL).getController();
    }

    private boolean validateFields()
    {
        return StringUtils.isEmpty(usernameField.getText()) || StringUtils.isEmpty(passwordField.getText());
    }
}
