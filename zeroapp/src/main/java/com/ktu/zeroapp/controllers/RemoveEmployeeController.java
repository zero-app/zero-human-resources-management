package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.RegistrationCode;
import com.ktu.zeroapp.entities.User;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.services.UserService;
import com.ktu.zeroapp.utils.AlertUtil;
import com.ktu.zeroapp.utils.EmployeeCondition;
import com.ktu.zeroapp.utils.Loader;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class RemoveEmployeeController
{
    private final EmployeeServices employeeServices = Main.getContext().getBean(EmployeeServices.class);
    private final UserService userService = Main.getContext().getBean(UserService.class);
    private final Loader loader = new Loader();

    private Employee employee;

    @FXML
    private TextField nameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private CheckBox addToBlackListBox;
    @FXML
    private TextArea commentArea;

    public void initialize(Employee employee)
    {
        this.employee = employee;

        nameField.setText(employee.getFirstName());
        lastNameField.setText(employee.getLastName());
    }

    @FXML
    private void cancel()
    {
        loader.goTo(Loader.EMPLOYEE_LIST_WINDOW_URL);
    }

    @FXML
    private void execute()
    {
        if(AlertUtil.confirmationAlert("Confirmation", "Are you sure you want to proceed?", ""))
        {
            employee.setCondition(EmployeeCondition.RESIGNED.getName());
            if(addToBlackListBox.isSelected())
            {
                employeeServices.blacklistEmployee(employee, commentArea.getText());
            }

            RegistrationCode registrationCode = userService.getRegistrationCode(employee);
            if(registrationCode != null)
            {
                userService.deleteRegistrationCode(registrationCode);
            }
            else
            {
                User user = userService.getUser(employee.getId());
                if(user != null)
                {
                    userService.deleteUser(user);
                }
            }

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "The operation was successful!");
            loader.goTo(Loader.EMPLOYEE_LIST_WINDOW_URL);
        }

    }
}
