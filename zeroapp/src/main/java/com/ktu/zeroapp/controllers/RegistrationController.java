package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.Main;
import com.ktu.zeroapp.entities.Employee;
import com.ktu.zeroapp.entities.RegistrationCode;
import com.ktu.zeroapp.entities.User;
import com.ktu.zeroapp.services.EmployeeServices;
import com.ktu.zeroapp.services.UserService;
import com.ktu.zeroapp.utils.*;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class RegistrationController
{
    private final Loader loader = new Loader();
    private final UserService userService = Main.getContext().getBean(UserService.class);
    private final EmployeeServices employeeServices = Main.getContext().getBean(EmployeeServices.class);

    private RegistrationCode registrationCode;
    private StringBuilder notificationText = new StringBuilder();

    @FXML
    private TextField registrationCodeField;
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private PasswordField repeatedPasswordField;

    @FXML
    private void register()
    {
        notificationText.setLength(0);

        if(validate() && validateInDatabase())
        {
            User user = new User();
            user.setEmployeeId(registrationCode.getEmployeeId());
            user.setUsername(usernameField.getText());
            user.setPassword(passwordField.getText());
            user.setRole(UserRole.EMPLOYEE.getRoleValue());

            userService.saveNewUser(user);
            userService.deleteRegistrationCode(registrationCode);

            Employee employee = employeeServices.getEmployee(user.getEmployeeId());
            employee.setCondition(EmployeeCondition.ACTIVE_IDENTIFICATION.getName());
            employeeServices.saveEmployee(employee);

            AlertUtil.displayAlert(Alert.AlertType.INFORMATION, "Success", "Registration was successful!");
            loader.goTo(Loader.LOGIN_WINDOW_URL);
        }
        else
        {
            AlertUtil.displayAlert(Alert.AlertType.WARNING,"Input error","There was an error with your input", notificationText.toString());
        }
    }

    private boolean validate()
    {
        boolean isCorrect = true;
        if(registrationCodeField.getText().length() != 10 || !RegexUtil.checkIfAlphaNumeric(registrationCodeField.getText()))
        {
            buildNotification("Registration code is incorrect");
            isCorrect = false;
        }
        if(usernameField.getText().length() < 4 || !RegexUtil.checkIfAlphaNumeric(usernameField.getText()))
        {
            buildNotification("Username is incorrect");
            isCorrect = false;
        }
        if(passwordField.getText().length() < 4)
        {
            buildNotification("Password is too short");
            isCorrect = false;
        }
        if(!repeatedPasswordField.getText().equals(passwordField.getText()) || repeatedPasswordField.getText().length() < 4)
        {
            buildNotification("Passwords do not match or too short");
            isCorrect = false;
        }

        return isCorrect;
    }

    private boolean validateInDatabase()
    {
        boolean isCorrect = true;
        if((registrationCode = userService.getRegistrationCode(registrationCodeField.getText())) == null)
        {
            buildNotification("Registration code does not exists");
            isCorrect = false;
        }
        if(userService.getUser(usernameField.getText()) != null)
        {
            buildNotification("This username is taken");
            isCorrect = false;
        }

        return isCorrect;
    }

    @FXML
    private void toLogin()
    {
        loader.goTo(Loader.LOGIN_WINDOW_URL);
    }

    private void buildNotification(String s)
    {
        notificationText.append(s);
        notificationText.append("\n");
    }
}
