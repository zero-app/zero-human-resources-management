package com.ktu.zeroapp.controllers;

import com.ktu.zeroapp.utils.AuthenticationUtil;
import com.ktu.zeroapp.utils.Loader;
import javafx.fxml.FXML;

public class MenuController
{
    private final Loader loader = new Loader();

    @FXML
    private void exit() {
        System.exit(0);
    }

    @FXML
    private void showEmployeeList() {
        loader.goTo(Loader.EMPLOYEE_LIST_WINDOW_URL);
    }

    @FXML
    private void showBlackEmployeeList() {
        loader.goTo(Loader.BLACK_LISTED_EMPLOYEES_URL);
    }

    @FXML
    private void toAddNewEmployee() {
        loader.goTo(Loader.ADD_EMPLOYEE_WINDOW_URL);
    }

    public void requestVacation() {
        loader.goTo(Loader.REQUEST_VACATION_WINDOW_URL);
    }

    @FXML
    private void showReviewList() {
        loader.goTo(Loader.REVIEW_LIST_WINDOW_URL);
    }

    @FXML
    private void logout()
    {
        AuthenticationUtil.logoutActiveUser();
        loader.goTo(Loader.LOGIN_WINDOW_URL);
        loader.unloadMenu();
    }

    public void viewSubmittedVacationRequests() {
        loader.goTo(Loader.MANAGE_SUBMITTED_VACATION_REQUESTS_WINDOW_URL);
    }
}
