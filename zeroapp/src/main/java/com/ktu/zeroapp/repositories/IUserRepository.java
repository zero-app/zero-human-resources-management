package com.ktu.zeroapp.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ktu.zeroapp.entities.User;

@Repository
public interface IUserRepository extends CrudRepository<User,Long> {
	List<User> findUserByUsername(String username);
	User findUserByEmployeeId(Long employeeId);
}
