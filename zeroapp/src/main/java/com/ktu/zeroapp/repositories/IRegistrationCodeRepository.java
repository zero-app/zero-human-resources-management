package com.ktu.zeroapp.repositories;

import com.ktu.zeroapp.entities.RegistrationCode;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRegistrationCodeRepository extends CrudRepository<RegistrationCode, Long>
{
    @Query("from RegistrationCode i where i.employeeId= :employeeId")
    RegistrationCode getRegistrationCodeByEmployeeId(@Param(value = "employeeId") Long employeeId);

    @Query("from RegistrationCode i where i.code= :code")
    RegistrationCode getRegistrationCodeByCode(@Param(value = "code") String code);
}
