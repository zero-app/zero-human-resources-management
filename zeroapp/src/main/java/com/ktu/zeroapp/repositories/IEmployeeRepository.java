package com.ktu.zeroapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ktu.zeroapp.entities.Employee;

@Repository
public interface IEmployeeRepository extends CrudRepository<Employee, Long> {
	List<Employee> findAll();

	@Query("select e from Employee e where e.id in (:ids)")
    List<Employee> findAllByIds(@Param("ids") List<Long> employeeIds);

	List<Employee> findAllByCondition(String condition);
}
