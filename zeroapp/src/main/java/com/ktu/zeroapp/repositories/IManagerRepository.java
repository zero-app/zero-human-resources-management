package com.ktu.zeroapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ktu.zeroapp.entities.Manager;

@Repository
public interface IManagerRepository extends CrudRepository<Manager, Long> {
}
