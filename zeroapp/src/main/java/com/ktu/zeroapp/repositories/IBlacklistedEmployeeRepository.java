package com.ktu.zeroapp.repositories;

import com.ktu.zeroapp.viewModels.BlacklistedEmployeeViewModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ktu.zeroapp.entities.BlacklistedEmployee;

import java.util.List;

@Repository
public interface IBlacklistedEmployeeRepository extends CrudRepository<BlacklistedEmployee, Long>{
    List<BlacklistedEmployee> findAll();
}
