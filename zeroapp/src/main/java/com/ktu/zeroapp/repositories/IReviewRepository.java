package com.ktu.zeroapp.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ktu.zeroapp.entities.Review;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface IReviewRepository extends CrudRepository<Review, Long> {

	List<Review> findAll();

	@Query("select r from Review r where r.dateOfReview between :start and :to")
    List<Review> findAllByDateOfReviewWithinDateRange(@Param("start") LocalDate from, @Param("to") LocalDate to);
}
