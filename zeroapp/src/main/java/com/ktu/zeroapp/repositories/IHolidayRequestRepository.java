package com.ktu.zeroapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ktu.zeroapp.entities.HolidayRequest;

import java.util.List;

@Repository
public interface IHolidayRequestRepository extends CrudRepository<HolidayRequest,Long> {
    List<HolidayRequest> findAll();
}
