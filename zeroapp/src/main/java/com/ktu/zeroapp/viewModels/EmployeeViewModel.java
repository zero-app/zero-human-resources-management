package com.ktu.zeroapp.viewModels;

import com.ktu.zeroapp.entities.Employee;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EmployeeViewModel {
	private Long id;
	private StringProperty name;
	private StringProperty lastName;
	private StringProperty address;
	private StringProperty dateStarted;
	private StringProperty phone;
	private DoubleProperty salary;
	private DoubleProperty holiday;
	private StringProperty email;

	public EmployeeViewModel(Employee employee) {
		setId(employee.getId());
		setName(employee.getFirstName());
		setLastName(employee.getLastName());
		setAddress(employee.getAddress());
		setPhone(employee.getPhoneNumber());
		setSalary(employee.getSalary());
		setHoliday(employee.getHoliday());
		setEmail(employee.getEmail());
		setDateStarted(employee.getStartedWork().toString());
	}

	public String getAddress() {
		return addressProperty().get();
	}

	public StringProperty addressProperty() {
		if(address == null){
			address = new SimpleStringProperty();
		}
		return address;
	}

	public void setAddress(String address) {
		this.addressProperty().set(address);
	}

	public String getDateStarted() {
		return dateStartedProperty().get();
	}

	public StringProperty dateStartedProperty() {
		if(dateStarted == null){
			dateStarted = new SimpleStringProperty();
		}
		return dateStarted;
	}

	public void setDateStarted(String dateStarted) {
		this.dateStartedProperty().set(dateStarted);
	}

	public String getPhone() {
		return phoneProperty().get();
	}

	public StringProperty phoneProperty() {
		if(phone == null){
			phone = new SimpleStringProperty();
		}
		return phone;
	}

	public void setPhone(String phone) {
		this.phoneProperty().set(phone);
	}

	public double getSalary() {
		return salaryProperty().get();
	}

	public DoubleProperty salaryProperty() {
		if (salary == null){
			salary = new SimpleDoubleProperty();
		}
		return salary;
	}

	public void setSalary(double salary) {
		this.salaryProperty().set(salary);
	}

	public double getHoliday() {
		return holidayProperty().get();
	}

	public DoubleProperty holidayProperty() {
		if (holiday == null){
			holiday = new SimpleDoubleProperty();
		}
		return holiday;
	}

	public void setHoliday(double holiday) {
		this.holidayProperty().set(holiday);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return nameProperty().get();
	}

	public StringProperty nameProperty() {
		if(name == null){
			name = new SimpleStringProperty();
		}
		return name;
	}

	public String getLastName() {
		return lastNameProperty().get();
	}

	public StringProperty lastNameProperty() {
		if(lastName == null){
			lastName = new SimpleStringProperty();
		}
		return lastName;
	}

	public void setName(String name) {
		this.nameProperty().set(name);
	}

	public void setLastName(String lastName) {
		this.lastNameProperty().set(lastName);
	}

	public StringProperty emailProperty()
	{
		if(email == null)
		{
			email = new SimpleStringProperty();
		}
		return email;
	}

	public void setEmail(String email)
	{
		this.emailProperty().set(email);
	}

	public String getEmail()
	{
		return this.emailProperty().get();
	}
}
