package com.ktu.zeroapp.viewModels;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public class BlacklistedEmployeeViewModel {

    private LongProperty id;

    private StringProperty firstName;
    private StringProperty lastName;
    private LocalDate startedEmployment;
    private LocalDate endedEmployment;
    private LocalDate addedToBlacklistOn;
    private StringProperty comment;

    public BlacklistedEmployeeViewModel(Long id, String firstName, String lastName, LocalDate startedWork, LocalDate blacklistedOn, String comment) {
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setStartedEmployment(startedWork);
        this.setEndedEmployment(blacklistedOn);
        this.setAddedToBlacklistOn(blacklistedOn);
        this.setComment(comment);
    }

    public long getId() {
        return idProperty().get();
    }

    public LongProperty idProperty() {
        if(id == null){
            id = new SimpleLongProperty();
        }
        return id;
    }

    public String getFirstName() {
        return firstNameProperty().get();
    }

    public StringProperty firstNameProperty() {
        if(firstName == null){
            firstName = new SimpleStringProperty();
        }
        return firstName;
    }

    public String getLastName() {
        return lastNameProperty().get();
    }

    public StringProperty lastNameProperty() {
        if(lastName == null) {
            lastName = new SimpleStringProperty();
        }
        return lastName;
    }

    public LocalDate getStartedEmployment() {
        return startedEmployment;
    }

    public LocalDate getEndedEmployment() {
        return endedEmployment;
    }

    public LocalDate getAddedToBlacklistOn() {
        return addedToBlacklistOn;
    }

    public String getComment() {
        return commentProperty().get();
    }

    public StringProperty commentProperty() {

        if(comment == null) {
            comment = new SimpleStringProperty();
        }
        return comment;
    }

    public void setId(long id) {
        this.idProperty().set(id);
    }

    public void setFirstName(String firstName) {
        this.firstNameProperty().set(firstName);
    }

    public void setLastName(String lastName) {
        this.lastNameProperty().set(lastName);
    }

    public void setStartedEmployment(LocalDate startedEmployment) {
        this.startedEmployment = startedEmployment;
    }

    public void setEndedEmployment(LocalDate endedEmployment) {
        this.endedEmployment = endedEmployment;
    }

    public void setAddedToBlacklistOn(LocalDate addedToBlacklistOn) {
        this.addedToBlacklistOn = addedToBlacklistOn;
    }

    public void setComment(String comment) {
        this.commentProperty().set(comment);
    }

    public StringProperty startedEmploymentProperty()
    {
        return new SimpleStringProperty(startedEmployment.toString());
    }

    public StringProperty endedEmploymentProperty()
    {
        return new SimpleStringProperty(endedEmployment.toString());
    }

    public StringProperty addedToBlacklistOnProperty()
    {
        return new SimpleStringProperty(addedToBlacklistOn.toString());
    }
}
