package com.ktu.zeroapp.viewModels;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HolidayRequestViewModel {

    private StringProperty fullName;
    private StringProperty vacationStartDate;
    private StringProperty vacationEndDate;
    private StringProperty comment;
    private StringProperty status;
    private LongProperty id;

    public HolidayRequestViewModel(Long id, String fullName, String vacationStart, String vacationEnd, String comment, String status) {
        this.setId(id);
        this.setFullName(fullName);
        this.setVacationStartDate(vacationStart);
        this.setVacationEndDate(vacationEnd);
        this.setComment(comment);
        this.setStatus(status);
    }

    public String getVacationStartDate() {
        return vacationStartDate.get();
    }

    public void setVacationStartDate(String vacationStartDate) {
        this.vacationStartDateProperty().set(vacationStartDate);
    }

    public StringProperty vacationStartDateProperty() {
        return vacationStartDate != null ? vacationStartDate : (vacationStartDate = new SimpleStringProperty());
    }

    public String getVacationEndDate() {
        return vacationEndDate.get();
    }

    public void setVacationEndDate(String vacationEndDate) {
        this.vacationEndDateProperty().set(vacationEndDate);
    }

    public StringProperty vacationEndDateProperty() {
        return vacationEndDate != null ? vacationEndDate : (vacationEndDate = new SimpleStringProperty());
    }

    public LongProperty idProperty() {
        if (id == null) {
            id = new SimpleLongProperty();
        }
        return id;
    }

    public Long getId() {
        return idProperty().getValue();
    }

    public void setId(Long id) {
        idProperty().setValue(id);
    }

    public String getFullName() {
        return fullNameProperty().get();
    }

    public void setFullName(String fullName) {
        this.fullNameProperty().set(fullName);
    }

    public StringProperty fullNameProperty() {
        if (fullName == null) {
            fullName = new SimpleStringProperty();
        }
        return fullName;
    }

    public String getComment() {
        return commentProperty().get();
    }

    public void setComment(String comment) {
        this.commentProperty().set(comment);
    }

    public StringProperty commentProperty() {
        if (comment == null) {
            comment = new SimpleStringProperty();
        }
        return comment;
    }

    public String getStatus() {
        return statusProperty().get();
    }

    public void setStatus(String status) {
        this.statusProperty().set(status);
    }

    public StringProperty statusProperty() {
        if (status == null) {
            status = new SimpleStringProperty();
        }
        return status;
    }

}
