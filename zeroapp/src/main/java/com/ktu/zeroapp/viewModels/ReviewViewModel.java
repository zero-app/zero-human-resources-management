package com.ktu.zeroapp.viewModels;

import com.ktu.zeroapp.entities.Review;
import javafx.beans.property.*;

public class ReviewViewModel {
	private LongProperty id;
	private StringProperty name;
	private StringProperty dateStringProperty;
	private IntegerProperty qualityProperty;
	private IntegerProperty gradeProperty;
	private IntegerProperty improvementProperty;
	private StringProperty commentProperty;

	public ReviewViewModel(String name, String dateString, Integer quality, Integer grade, Integer improvement, String comment) {
		this.name = new SimpleStringProperty(name);
		this.dateStringProperty = new SimpleStringProperty(dateString);
		this.qualityProperty = new SimpleIntegerProperty(quality);
		this.gradeProperty = new SimpleIntegerProperty(grade);
		this.improvementProperty = new SimpleIntegerProperty(improvement);
		this.commentProperty = new SimpleStringProperty(comment);
	}

	public ReviewViewModel(Review i, String name) {
		this.setId(i.getId());
		this.name = new SimpleStringProperty(name);
		this.dateStringProperty = new SimpleStringProperty(i.getDateOfReview().toString());
		this.qualityProperty = new SimpleIntegerProperty(i.getQuality());
		this.gradeProperty = new SimpleIntegerProperty(i.getGrade());
		this.improvementProperty = new SimpleIntegerProperty(i.getImprovement());
		this.commentProperty = new SimpleStringProperty(i.getComment());
	}

	public long getId() {
		return idProperty().get();
	}

	public LongProperty idProperty() {
		if(id == null){
			id = new SimpleLongProperty();
		}
		return id;
	}

	public void setId(long id) {
		this.idProperty().set(id);
	}

	public String getName() {
		return name.get();
	}

	public StringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getDateStringProperty() {
		return dateStringProperty.get();
	}

	public StringProperty dateStringPropertyProperty() {
		return dateStringProperty;
	}

	public void setDateStringProperty(String dateStringProperty) {
		this.dateStringProperty.set(dateStringProperty);
	}

	public int getQualityProperty() {
		return qualityProperty.get();
	}

	public IntegerProperty qualityPropertyProperty() {
		return qualityProperty;
	}

	public void setQualityProperty(int qualityProperty) {
		this.qualityProperty.set(qualityProperty);
	}

	public int getGradeProperty() {
		return gradeProperty.get();
	}

	public IntegerProperty gradePropertyProperty() {
		return gradeProperty;
	}

	public void setGradeProperty(int gradeProperty) {
		this.gradeProperty.set(gradeProperty);
	}

	public int getImprovementProperty() {
		return improvementProperty.get();
	}

	public IntegerProperty improvementPropertyProperty() {
		return improvementProperty;
	}

	public void setImprovementProperty(int improvementProperty) {
		this.improvementProperty.set(improvementProperty);
	}

	public String getCommentProperty() {
		return commentProperty.get();
	}

	public StringProperty commentPropertyProperty() {
		return commentProperty;
	}

	public void setCommentProperty(String commentProperty) {
		this.commentProperty.set(commentProperty);
	}
}
