package com.ktu.zeroapp.utils;

import com.ktu.zeroapp.entities.User;
import com.ktu.zeroapp.repositories.IUserRepository;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationUtil
{
	private Log log = LogFactory.getLog(AuthenticationUtil.class);

	@Value("${salt}")
	private static String salt;

	@Autowired
	IUserRepository userRepository;

    private static User loggedUser;

    public static User getLoggedUser()
    {
        return loggedUser;
    }

    public boolean validate(Long id, String password){
		User user = userRepository.findOne(id);
			String userPass1 = user.getPassword();
			String userPass2 = encrypt(password);
			if(userPass1.equals(userPass2)){
				loggedUser = user;
				return true;
			}
		return false;
	}

	public String encrypt(String password){
		byte[] hash = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			password += salt;
			hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));

		} catch (NoSuchAlgorithmException e) {
			log.error("Error when hashing password",e);
		}
		return new String(hash);
	}

	public static Long getEmployeeId() {
		return loggedUser.getId();
	}

	public static void logoutActiveUser()
	{
		loggedUser = null;
	}
}
