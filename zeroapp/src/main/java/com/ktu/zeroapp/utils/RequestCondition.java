package com.ktu.zeroapp.utils;

public enum RequestCondition {
    REQUEST_APPROVED("approved",1),
    REQUEST_DECLINED("declined",2);

    private String name;
    private Integer value;
    RequestCondition(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public Integer getValue(){
        return value;
    }

    public String getName(){
        return name;
    }
}
