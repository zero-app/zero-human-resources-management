package com.ktu.zeroapp.utils;

import com.ktu.zeroapp.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public class Loader
{
    public static final String ADMIN_MENU_URL = "/layout/AdminMenu.fxml";
    public static final String EMPLOYEE_MENU_URL = "/layout/EmployeeMenu.fxml";

    public static final String LOGIN_WINDOW_URL = "/layout/LoginWindow.fxml";
    public static final String REGISTRATION_WINDOW_URL = "/layout/RegistrationWindow.fxml";
    public static final String WELCOME_WINDOW_URL = "/layout/WelcomeWindow.fxml";
    public static final String EMPLOYEE_LIST_WINDOW_URL = "/layout/EmployeeListWindow.fxml";
    public static final String ADD_EMPLOYEE_WINDOW_URL = "/layout/AddEmployeeWindow.fxml";
    public static final String REVIEW_WINDOW_URL = "/layout/ReviewWindow.fxml";
    public static final String REVIEW_LIST_WINDOW_URL = "/layout/ReviewListWindow.fxml";
    public static final String REMOVE_EMPLOYEE_WINDOW_URL = "/layout/RemoveEmployeeWindow.fxml";
    public static final String REQUEST_VACATION_WINDOW_URL = "/layout/RequestVacationWindow.fxml";
    public static final String MANAGE_SUBMITTED_VACATION_REQUESTS_WINDOW_URL = "/layout/ManageSubmittedVacationRequestsWindow.fxml";
    public static final String BLACK_LISTED_EMPLOYEES_URL = "/layout/BlackListedEmployeeWindow.fxml";

    public FXMLLoader goTo(String location)
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(location));
        try
        {
            Parent pane = loader.load();
            Main.getRoot().setCenter(pane);

            return loader;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public void loadMenu(String location)
    {
        try
        {
            Main.getRoot().setTop(FXMLLoader.load(getClass().getResource(location)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void unloadMenu()
    {
        Main.getRoot().setTop(null);
    }
}
