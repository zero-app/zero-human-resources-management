package com.ktu.zeroapp.utils;

public enum UserRole {
    OWNER("Owner",20),
    ADMINISTRATOR("Administrator",10),
    EMPLOYEE("Employee",1);

    private String roleName;
    private Integer roleValue;
    UserRole(String roleName, Integer roleValue){
        this.roleName = roleName;
        this.roleValue = roleValue;
    }

    public String getRoleName(){
        return this.roleName;
    }

    public Integer getRoleValue(){
        return this.roleValue;
    }
}
