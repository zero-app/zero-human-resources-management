package com.ktu.zeroapp.utils;

public enum ResponseCode {
	USER_DOES_NOT_EXIST(1,"Username does not exist"),
	INCORRECT_PASSWORD(2, "Password is incorrect"),
	SUCCESSFUL_LOGIN(3, "Login was successful");

	private int code;
	private String message;

	ResponseCode(int v, String message) {
		this.code = v;
		this.message = message;
	}

	public int getCode(){
		return code;
	}

	public String getMessage() {
		return message;
	}
}
