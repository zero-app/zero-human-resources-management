package com.ktu.zeroapp.utils;

public enum EmployeeCondition {

    ACTIVE("active", 1),
    INACTIVE("inactive", 2),
    BLACKLISTED("blacklisted",3),
    RESIGNED("resigned",4),
    SUSPENDED("suspended",5),
    INACTIVE_IDENTIFICATION("expired",6),
    ACTIVE_IDENTIFICATION("active",7);

    private String name;
    private Integer value;

    EmployeeCondition(String name, Integer value){
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
