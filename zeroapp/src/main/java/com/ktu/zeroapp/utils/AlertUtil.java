package com.ktu.zeroapp.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;

import java.util.Optional;

import com.ktu.zeroapp.Main;

public class AlertUtil
{
    public static void displayAlert(Alert.AlertType alertType, String title, String header, String content)
    {
        Alert alert = new Alert (alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(Main.getPrimaryStage());
        alert.show();
    }

    public static void displayAlert(Alert.AlertType alertType, String title, String header)
    {
        Alert alert = new Alert (alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(Main.getPrimaryStage());
        alert.show();
    }

    public static void displayAlert(Alert.AlertType alertType, String title)
    {
        Alert alert = new Alert (alertType);
        alert.setTitle(title);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(Main.getPrimaryStage());
        alert.show();
    }

    public static boolean confirmationAlert(String title, String header, String content)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(Main.getPrimaryStage());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
