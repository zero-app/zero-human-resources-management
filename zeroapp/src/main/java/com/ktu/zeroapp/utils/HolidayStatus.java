package com.ktu.zeroapp.utils;

public enum HolidayStatus {
    WAITING("Waiting"),
    APPROVED("Approved"),
    DECLINED("Declined");

    private String name;

    HolidayStatus(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
