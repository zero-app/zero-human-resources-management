package com.ktu.zeroapp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil
{
    private static final Pattern VALID_EMAIL_ADDRESS_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_ONLY_LETTERS_PATTERN = Pattern.compile("^[a-zA-Zą-žĄ-Ž]+$");
    private static final Pattern VALID_ONLY_NUMBERS_PATTERN = Pattern.compile("^\\d+(\\.\\d+)?$");
    private static final Pattern VALID_INTEGER_PATTERN = Pattern.compile("^[0-9]+$");
    private static final Pattern VALID_PHONE_NUMBER_PATTERN = Pattern.compile("^[+]?\\d+$");
    private static final Pattern VALID_FULL_NAME_PATTERN = Pattern.compile("^[a-zA-Z]+[ \t][a-zA-Z]+");
    private static final Pattern VALID_WEBSITE_PATTERN = Pattern.compile("^(http[s]?://)?[a-zA-Z]+[.](net|com|se|ru|lt|org)");
    private static final Pattern VALID_ALPHA_NUMERIC_PATTERN = Pattern.compile("^[0-9a-zA-Z]+$");

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static boolean checkIfAlphaNumeric(String stringToCheck)
    {
        return VALID_ALPHA_NUMERIC_PATTERN.matcher(stringToCheck).find();
    }

    public static boolean checkIfFullNamePatterFound(String stringToCheck) {
        return VALID_FULL_NAME_PATTERN.matcher(stringToCheck).find();
    }

    public static boolean checkIfWebsitePatterFound(String stringToCheck) {
        return VALID_WEBSITE_PATTERN.matcher(stringToCheck).find();
    }

    public static boolean checkIfEmail(String stringToCheck)
    {
        Matcher matcher = VALID_EMAIL_ADDRESS_PATTERN.matcher(stringToCheck);
        return matcher.find();
    }

    public static boolean checkIfOnlyLetters(String stringToCheck)
    {
        Matcher matcher = VALID_ONLY_LETTERS_PATTERN.matcher(stringToCheck);
        return matcher.find();
    }

    public static boolean checkIfOnlyNumbers(String stringToCheck)
    {
        Matcher matcher = VALID_ONLY_NUMBERS_PATTERN.matcher(stringToCheck);
        return matcher.find();
    }

    public static boolean checkIfInteger(String stringToCheck)
    {
        Matcher matcher = VALID_INTEGER_PATTERN.matcher(stringToCheck);
        return matcher.find();
    }

    public static boolean checkIfDate(String stringToCheck)
    {
        try
        {
            String formatted = DATE_FORMAT.format(DATE_FORMAT.parse(stringToCheck));

            return formatted.equals(stringToCheck);

        }
        catch (ParseException e)
        {
            return false;
        }
    }

    public static boolean checkIfPhoneNumber(String stringToCheck)
    {
        Matcher matcher = VALID_PHONE_NUMBER_PATTERN.matcher(stringToCheck);
        return matcher.find();
    }
}
