package com.ktu.zeroapp.utils;

public class ConstantValues {
	public static final Double MEDIUM_LABEL_WIDTH = 150.0;
	public static final Double MEDIUM_FIELD_WIDTH = 200.0;
	public static final Double PREF_WINDOW_WIDTH = 500.0;
	public static final Double PREF_WINDOW_HEIGHT = 500.0;
	public static final String STATUS_ACTIVE = "active";
	public static final String STATUS_INACTIVE = "inactive";
	public static final String REQUEST_CONDITION_WAITING = "waiting";
	public static final String REQUEST_CONDITION_APPROVED = "approved";
	public static final String REQUEST_CONDITION_DECLINED = "declined";



}
